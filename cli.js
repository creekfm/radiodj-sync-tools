#!/usr/bin/env node

/*

CLI for synchronizing RadioDJ data from a master database to slave databases.

*/

var fs = require('fs-extra');
var path = require('path');
var program = require('commander');
var chalk = require('chalk');

var db = require('./db.js');

//Include modules from ./lib
var c = require('./lib/common');
var settings = require('./lib/settings').get();

//Sync commands here:
var sync_commands = require('./sync');

//------------------------------------------------------------
//
// Explanations for each of these are inside the sync.js file.
//
//------------------------------------------------------------

program
  .command('update')
  .action(function(x){

    console.log("Syncing from master DB to slave DB.");

    sync_commands.updateSongs();

  });

program
  .command('add-new')
  .action(function(x){

    console.log("Adding any new tracks on master DB to slave DBs.");

    sync_commands.addNewSongs();

  });

program
  .command('reset')
  .action(function(x){

    console.log("Resetting songs tables on slave DBs, and adding rows from master SB.");

    sync_commands.resetSongsTables();

  });

program
  .command('add-files')
  .action(function(x){

    console.log("Adding new files in the main music folder to the master DB.");

    sync_commands.addFiles();

  });

program
  .command('convert-selector-playlists')
  .action(function(x){

    console.log("Converting playlists in Selector playlists folder to M3U files for RadioDJ.");

    sync_commands.convertPlaylistsFromSelector

  })


program.parse(process.argv);
