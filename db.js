
//Set up the database with a new Sequalize object and settings from the setting.json file.

/*

!!!

This... might not yet run.

*/


var settings = require('./lib/settings').get();
var Sequelize = require("sequelize");
var mysql = require('mysql');

var Sequelize = require("sequelize");

//Create the buld function that ingest the live database connection.
// Parameters:
// - db: Sequelize Object [Required]
module.exports.build = function(which_one, db_connection){

  return db_connection.define("songs", {
    cue_times: {
      type: Sequelize.STRING,
      defaultValue: ""
    },
    path: {
      type: Sequelize.STRING,
      defaultValue: ""
    },
    precise_cue: {
      type: Sequelize.BOOLEAN,
      defaultValue: ""
    },
    fade_type: {
      type: Sequelize.BOOLEAN,
      defaultValue: ""
    },
    start_type: {
      type: Sequelize.BOOLEAN,
      defaultValue: ""
    },
    end_type: {
      type: Sequelize.BOOLEAN,
      defaultValue: ""
    },
    mix_type: {
      type: Sequelize.BOOLEAN,
      defaultValue: ""
    }
  },
  {

    timestamps: false,

    // Model tableName will be the same as the model name
    freezeTableName: true,

    indexes: [
      {
        unique: true,
        fields: "unique_id"
      }
    ]

  });

}


var sequelize_instances = {

  master: {},

  slaves: {}

}


//Set up master
var sequelize_instances.master = new Sequelize(settings.mysql.master.db_name, settings.mysql.master.username, settings.mysql.master.password, {
  host: settings.mysql.master.host,
  logging: function(x){ return true; },
  defaultValue: ""
});

//Export the sequelize live connection object.
module.exports.sequelize_master = sequelize_master;


//Set up slaves
for (var slave_id in object) {
  if (object.hasOwnProperty(slave_id)) {

    var sequelize_instances.slaves[slave_id] = new Sequelize(settings.mysql.slaves[slave_id].db_name, settings.mysql.slaves[slave_id].username, settings.mysql.slaves[slave_id].password, {
      host: settings.mysql.slaves[slave_id].host,
      logging: function(x){ return true; },
      defaultValue: ""
    });

    //Export the sequelize live connection object.
    module.exports.sequelize.slaves[slave_id] = sequelize_instances.slaves[slave_id];

  }
}



//Make DB connections available
module.exports.master = module.exports.build("master", sequelize_master1);
module.exports.slaves = {
  slave1: module.exports.build("slave1", sequelize_slave1),
  slave2: module.exports.build("slave2", sequelize_slave1)
}
