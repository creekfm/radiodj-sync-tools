/*

Main functions for the sync tools.

*/

var fs = require('fs-extra');
var path = require('path');
var chalk = require('chalk');

var db = require('./db.js');

//Include modules from ./lib
var c = require('./lib/common');
var settings = require('./lib/settings').get();


/*

UPDATE "songs" tables on slaves with master.

Copy all of the columns for each row in the "songs" table to the same song in the slave DBs based on filename of the audio file. If it's the same filename, then we can assume that it's the same file.

*/
module.exports.updateSongs = function(){

  console.log("Syncing from master to slave RDJ databases.");

  /*


  */

  //Get the songs in the master database
  db.master

    .findAll()

    .then(function(rows_from_master) {

      var query_count = 0;
      var updated_count = 0;

      rows_from_master.forEach(function(row_from_master){

        query_count++;

        console.log("Row: "+query_count);
        console.log(row_from_db1.cue_times);

        var db2_path = settings.mysql.db2.path_prefix
          +row_from_db1.path.replace(settings.mysql.db1.path_prefix, '');

        console.log("--------------------------");

        /*
        For each SLAVE database, update the songs from this master DB.
        */
        async.eachOfSeries(db.slaves, function(slave_database_connection, callback{

          /*

          !!!

          This code below go into a shared function, like:

          DB.syncRow(row_from_master, slave_database_connection);

          ...then you can iterate over both in one function with the async module.

          */

          db.slaves[slave_id]

            .update(

              {

                /* !!!

                INSERT OTHER COLUMNS HERE.

                Or, is there a better way in Node sequelize module to just detect all columns rather than making them explicit here?

                If now, let's just list all of them out for now, below:

                */

                cue_times: row_from_db1.cue_times,
                precise_cue: row_from_db1.precise_cue,
                fade_type: row_from_db1.fade_type,
                start_type: row_from_db1.start_type,
                end_type: row_from_db1.end_type,
                mix_type: row_from_db1.mix_type
              },

              { where: { path: db2_path } }

            )
            .then(function(count, rows) {

              if(count > 0 || typeof rows != "undefined"){

                updated_count++;

                console.log(updated_count);

                console.log("++++++++++++++++++");
                console.log("DONE: " + db2_path);
                console.log("++++++++++++++++++");

              }

            })

        });


      })

    })

}

/*

REPLACE "songs" tables on slaves with master

*/
module.exports.resetSongsTables = function(){

  //Get the data from the master DB

    //For each slave DB:

      //Truncate this slave's song table.

      //Add ALL of the songs from master to this slave.

}

/*

ADD new songs from "songs" tables to slaves from master.

*/
module.exports.addNewSongs = function(){

  console.log("Adding any new tracks on master to slaves.");

  //Get the data from the master DB

    //For each slave DB:

      //Find new tracks that aren't yet in songs table on this slave DB.
      // - Get the newest song in the slave DB based on the date_added column.
      // - Then, add all the songs from master that are newer than the newest song on this slave DB.

}

/*

ADD FILES: Add the new audio files to the master database.

(Only on the local master.)

*/
module.exports.addFiles = function(){

  //Look for new files in the main music folder.

  // Path of main music folder is in: settings.master_music_path

  // Add these files to the master DB based on filename and file path.

  // Don't sync or anything to the slaves. That will be initialized separately in the UI.

}


module.exports.convertPlaylistsFromSelector = function(){

  //Run the (previously made) M3U synchronization tool.

  //Insert the code for the sync tool into this Node.js project here.

})


program.parse(process.argv);
